#include <assert.h>
#include "ring_buffer.h"


bool RingBuffer_Init(RingBuffer *ringBuffer, char *dataBuffer, size_t dataBufferSize) {
	assert(ringBuffer);
	assert(dataBuffer);
	assert(dataBufferSize > 0);
	
	if ((ringBuffer) && (dataBuffer) && (dataBufferSize > 0)) {
	    ringBuffer -> head = 0;
        ringBuffer -> tail = 0;
	    ringBuffer -> count = 0;
	    ringBuffer -> max_count = dataBufferSize;
	    ringBuffer -> buf = dataBuffer;
	    memset(dataBuffer, 0, dataBufferSize);
	    
	    return true;
	}
	
	return false;
}

bool RingBuffer_Clear(RingBuffer *ringBuffer) {
	assert(ringBuffer);
	
	if (ringBuffer) {
	    ringBuffer -> head = 0;
        ringBuffer -> tail = 0;
	    ringBuffer -> count = 0;
	    
	    return true;
	}
	
	return false;
}

bool RingBuffer_IsEmpty(const RingBuffer *ringBuffer) {
	assert(ringBuffer);	

	if (ringBuffer -> count == 0) {
	    return true;
	}
	
	return false;
}

size_t RingBuffer_GetLen(const RingBuffer *ringBuffer) {
	assert(ringBuffer);
	
	if (ringBuffer) {
		return ringBuffer -> count;
	}
	
	return 0;
}

size_t RingBuffer_GetCapacity(const RingBuffer *ringBuffer) {
	assert(ringBuffer);
	
	if (ringBuffer) {
		return ringBuffer -> max_count;
	}
	
	return 0;	
}


bool RingBuffer_PutChar(RingBuffer *ringBuffer, char c) {
	assert(ringBuffer);
	
	if (ringBuffer) {
		if (ringBuffer -> count < ringBuffer -> max_count) {
		    *(ringBuffer -> buf + ringBuffer -> head) = c;
		    ++(ringBuffer -> head); 
		    
			if (ringBuffer -> head == ringBuffer -> max_count) {
				ringBuffer -> head = 0;
			}
		        
		    ++(ringBuffer -> count); 
		        
		    return true;
		}
		
	}
	
	return false;
}

bool RingBuffer_GetChar(RingBuffer *ringBuffer, char *c) {
	assert(ringBuffer);
	assert(c);
	
	if ((ringBuffer) && (c)) {
		if (ringBuffer -> count > 0) {
		    char c_temp =  *(ringBuffer -> buf + ringBuffer -> tail);
		    *c = c_temp;
		    ++(ringBuffer -> tail); 
			
		    if (ringBuffer -> tail == ringBuffer -> max_count) {
		        ringBuffer -> tail = 0;
		    }
			
		    --(ringBuffer -> count); 
		        
		    return true;
		}
	}
	
	return false;
}
